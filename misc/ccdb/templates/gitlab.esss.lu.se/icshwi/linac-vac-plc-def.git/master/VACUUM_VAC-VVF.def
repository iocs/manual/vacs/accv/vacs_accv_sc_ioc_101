############################ Vacuum Fast Gate Valve #############################


#-############################
#- COMMAND BLOCK
#-############################

define_command_block()

add_digital("CloseManuCmd",                    PV_NAME="ManualCloseCmd",          PV_DESC="Manual CLOSE command")
add_digital("OpenManuCmd",                     PV_NAME="ManualOpenCmd",           PV_DESC="Manual OPEN command")
add_digital("SensorOffCmd",                    PV_NAME="SensorOffCmd",            PV_DESC="Turn gauges OFF command")
add_digital("SensorOnCmd",                     PV_NAME="SensorOnCmd",             PV_DESC="Turn gauges ON command")
add_digital("BlockClosedCmd",                  PV_NAME="AutoCloseDisCmd",         PV_DESC="Disable automatic CLOSE")
add_digital("BlockOpenCmd",                    PV_NAME="AutoOpenDisCmd",          PV_DESC="Disable automatic OPEN")
add_digital("UnlockClosedCmd",                 PV_NAME="AutoCloseEnaCmd",         PV_DESC="Enable automatic CLOSE")
add_digital("UnlockOpenCmd",                   PV_NAME="AutoOpenEnaCmd",          PV_DESC="Enable automatic OPEN")
add_digital("ResetCmd",                        PV_NAME="RstErrorsCmd",            PV_DESC="Reset errors and warnings")
add_digital("ClearCountersCmd",                PV_NAME="ClrCountersCmd",          PV_DESC="Clear counters")


#-############################
#- STATUS BLOCK
#-############################

define_status_block()

add_major_alarm("ErrorSt",  "Tripped",		   PV_NAME="ErrorR",                  PV_DESC="Error detected by the control function",  PV_ONAM="Error",                   PV_ZNAM="Healthy",          ARCHIVE=True)
#-----------------------------
add_digital("OpenDQSt",                        PV_NAME="OpenDQ-RB",               PV_DESC="Status of the OPEN digital output",       PV_ONAM="True",                 PV_ZNAM="False")
add_digital("CloseSt",                         PV_NAME="ClosedR",                 PV_DESC="The valve is CLOSED",                     PV_ONAM="Closed",                                              ARCHIVE=True)
add_digital("UndefinedSt",                     PV_NAME="UndefinedR",              PV_DESC="The valve is neither OPEN nor CLOSED",    PV_ONAM="Undefined")
add_digital("OpenSt",                          PV_NAME="OpenR",                   PV_DESC="The valve is OPEN",                       PV_ONAM="Open",                                                ARCHIVE=True)
add_digital("WarningSt",                       PV_NAME="WarningR",                PV_DESC="A warning is active",                     PV_ONAM="Warning")
add_digital("InvalidCommandSt",                PV_NAME="InvalidCommandR",         PV_DESC="Last command sent cannot be processed",   PV_ONAM="Invalid command")
add_digital("ValidSt",                         PV_NAME="ValidR",                  PV_DESC="Communication is valid",                  PV_ONAM="Valid",                PV_ZNAM="Invalid")

add_digital("InrushGauge1St",                  PV_NAME="Gauge1InrushR",           PV_DESC="Inrush detected on gauge 1",              PV_ONAM="INRUSH",               PV_ZNAM="Nominal",             ARCHIVE=True)
add_digital("ReadyGauge1St",                   PV_NAME="Gauge1ReadyR",            PV_DESC="Sensor module 1 detects no faults",       PV_ONAM="Ready",                PV_ZNAM="Faulty",              ARCHIVE=True)
add_digital("InrushGauge2St",                  PV_NAME="Gauge2InrushR",           PV_DESC="Inrush detected on gauge 2",              PV_ONAM="INRUSH",               PV_ZNAM="Nominal",             ARCHIVE=True)
add_digital("ReadyGauge2St",                   PV_NAME="Gauge2ReadyR",            PV_DESC="Sensor module 2 detects no faults",       PV_ONAM="Ready",                PV_ZNAM="Faulty",              ARCHIVE=True)

add_analog("Gauge1Press",          "REAL",     PV_NAME="Gauge1PressR",            PV_DESC="Gauge 1 pressure reading",                PV_EGU="mbar",                  ARCHIVE=True)
add_analog("Gauge2Press",          "REAL",     PV_NAME="Gauge2PressR",            PV_DESC="Gauge 2 pressure reading",                PV_EGU="mbar",                  ARCHIVE=True)

add_digital("SystemReadySt",                   PV_NAME="SystemReadyR",            PV_DESC="Controller detects no faults in system",  PV_ONAM="Ready",                PV_ZNAM="Faulty",              ARCHIVE=True)
add_digital("ValveReadySt",                    PV_NAME="ValveReadyR",             PV_DESC="Controller detects no faults in valve",   PV_ONAM="Ready",                PV_ZNAM="Faulty",              ARCHIVE=True)

add_digital("SensorDQSt",                      PV_NAME="SensorDQ-RB",             PV_DESC="Status of the SENSORS_OFF digital output",PV_ONAM="True",                 PV_ZNAM="False",               ARCHIVE=True)
add_digital("InputExternalDQSt",               PV_NAME="CloseTriggerDQ-RB",       PV_DESC="Status of the INPUT_EXTERNAL DO",         PV_ONAM="True",                 PV_ZNAM="False",               ARCHIVE=True)
add_digital("OpenBlockedSt",                   PV_NAME="AutoOpenDisStR",          PV_DESC="Automatic OPEN toggle status",            PV_ONAM="Auto open Disabled",   PV_ZNAM="Auto open Enabled",   ARCHIVE=True)
add_digital("CloseBlockedSt",                  PV_NAME="AutoCloseDisStR",         PV_DESC="Automatic CLOSE toggle status",           PV_ONAM="Auto close Disabled",  PV_ZNAM="Auto close Enabled",  ARCHIVE=True)

add_digital("PreviousInterlockNo1HealthySt",   PV_NAME="ITLck_Prev_1_HltyR",      PV_DESC="Previous interlock:1 is HEALTHY",         PV_ONAM="HEALTHY", ARCHIVE=True)
add_digital("PreviousInterlockNo1TrippedSt",   PV_NAME="ITLck_Prev_1_TrpR",       PV_DESC="Previous interlock:1 is TRIPPED",         PV_ONAM="TRIPPED", ARCHIVE=True)
add_digital("PreviousInterlockNo1BypassedSt",  PV_NAME="ITLck_Prev_1_OvRidnR",    PV_DESC="Previous interlock:1 is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Previous Interlock No1",  PV_NAME="ITLck_Prev_1_DisR",       PV_DESC="Previous interlock:1 is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("PreviousInterlockNo2HealthySt",   PV_NAME="ITLck_Prev_2_HltyR",      PV_DESC="Previous interlock:2 is HEALTHY",         PV_ONAM="HEALTHY", ARCHIVE=True)
add_digital("PreviousInterlockNo2TrippedSt",   PV_NAME="ITLck_Prev_2_TrpR",       PV_DESC="Previous interlock:2 is TRIPPED",         PV_ONAM="TRIPPED", ARCHIVE=True)
add_digital("PreviousInterlockNo2BypassedSt",  PV_NAME="ITLck_Prev_2_OvRidnR",    PV_DESC="Previous interlock:2 is OVERRIDEN",       PV_ONAM="OVERRIDEN")
add_digital("Disable Previous Interlock No2",  PV_NAME="ITLck_Prev_2_DisR",       PV_DESC="Previous interlock:2 is NOT CONFIGURED",  PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
#-----------------------------
add_digital("NextInterlockNo1HealthySt",       PV_NAME="ITLck_Next_1_HltyR",      PV_DESC="Next interlock:1 is HEALTHY",             PV_ONAM="HEALTHY", ARCHIVE=True)
add_digital("NextInterlockNo1TrippedSt",       PV_NAME="ITLck_Next_1_TrpR",       PV_DESC="Next interlock:1 is TRIPPED",             PV_ONAM="TRIPPED", ARCHIVE=True)
add_digital("NextInterlockNo1BypassedSt",      PV_NAME="ITLck_Next_1_OvRidnR",    PV_DESC="Next interlock:1 is OVERRIDEN",           PV_ONAM="OVERRIDEN")
add_digital("Disable Next Interlock No1",      PV_NAME="ITLck_Next_1_DisR",       PV_DESC="Next interlock:1 is NOT CONFIGURED",      PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
add_digital("NextInterlockNo2HealthySt",       PV_NAME="ITLck_Next_2_HltyR",      PV_DESC="Next interlock:2 is HEALTHY",             PV_ONAM="HEALTHY", ARCHIVE=True)
add_digital("NextInterlockNo2TrippedSt",       PV_NAME="ITLck_Next_2_TrpR",       PV_DESC="Next interlock:2 is TRIPPED",             PV_ONAM="TRIPPED", ARCHIVE=True)
add_digital("NextInterlockNo2BypassedSt",      PV_NAME="ITLck_Next_2_OvRidnR",    PV_DESC="Next interlock:2 is OVERRIDEN",           PV_ONAM="OVERRIDEN")
add_digital("Disable Next Interlock No2",      PV_NAME="ITLck_Next_2_DisR",       PV_DESC="Next interlock:2 is NOT CONFIGURED",      PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")
#-----------------------------
add_digital("OpenInterlockHealthySt",          PV_NAME="ITLck_Open_HltyR",        PV_DESC="Open interlock is HEALTHY",         		 PV_ONAM="HEALTHY", ARCHIVE=True)
add_digital("OpenInterlockTrippedSt",          PV_NAME="ITLck_Open_TrpR",         PV_DESC="Open interlock is TRIPPED",         		 PV_ONAM="TRIPPED", ARCHIVE=True)
add_digital("OpenInterlockBypassedSt",  	   PV_NAME="ITLck_Open_OvRidnR",      PV_DESC="Open interlock is OVERRIDEN",       		 PV_ONAM="OVERRIDEN")
add_digital("Disable Open Interlock",  		   PV_NAME="ITLck_Open_DisR",         PV_DESC="Open interlock is NOT CONFIGURED",  	 	 PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")

add_analog("WarningCode",          "BYTE",     PV_NAME="WarningCodeR",            PV_DESC="Active warning code", ARCHIVE=True)

add_analog("ErrorCode",            "BYTE",     PV_NAME="ErrorCodeR",              PV_DESC="Active error code", ARCHIVE=True)

add_analog("RunTime",              "REAL",     PV_NAME="RunTimeR",                PV_DESC="Runtime in hours", PV_EGU="h")

add_analog("OpenCounter",          "UDINT",    PV_NAME="OpenCounterR",            PV_DESC="Number of opens", ARCHIVE=True)

add_analog("Gauge1St",               "INT",    PV_NAME ="Gauge1R",                 PV_DESC="Gauge 1 overal status")
add_analog("Gauge2St",               "INT",    PV_NAME ="Gauge2R",                 PV_DESC="Gauge 2 overal status")

add_verbatim("""
record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_1_Device")
{
    field(DESC, "Device assigned to this interlock")
    field(VAL,  "[PLCF#ext.dash_means_empty("VVF_ITLck_Prev_1")]")
    field(PINI, "YES")
}


record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_2_Device")
{
    field(DESC, "Device assigned to this interlock")
    field(VAL,  "[PLCF#ext.dash_means_empty("VVF_ITLck_Prev_2")]")
    field(PINI, "YES")
}


record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_1_Device")
{
    field(DESC, "Device assigned to this interlock")
    field(VAL,  "[PLCF#ext.dash_means_empty("VVF_ITLck_Next_1")]")
    field(PINI, "YES")
}


record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_2_Device")
{
    field(DESC, "Device assigned to this interlock")
    field(VAL,  "[PLCF#ext.dash_means_empty("VVF_ITLck_Next_2")]")
    field(PINI, "YES")
}


record(stringin, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_Device")
{
    field(DESC, "Device assigned to this interlock")
    field(VAL,  "[PLCF#ext.dash_means_empty("VVF_ITLck_Open")]")
    field(PINI, "YES")
}
""")

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:#P1ITLckStatR")
{
    field(DESC, "Calculate combined interlock status")
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_1_TrpR CP")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_1_OvRidnR CP")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_1_HltyR CP")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_1_DisR CP")

# Recalculate Tripped; Check that only one is true
# Clear Tripped if Overriden and Set Tripped if Overriden w/out being tripped
    field(CALC, "A:=A!=B;E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
    field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
    field(DOPT, "Use OCAL")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStat_Prev_1-R PP")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:#P2ITLckStatR")
{
    field(DESC, "Calculate combined interlock status")
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_2_TrpR CP")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_2_OvRidnR CP")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_2_HltyR CP")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_Prev_2_DisR CP")

# Recalculate Tripped; Check that only one is true
# Clear Tripped if Overriden and Set Tripped if Overriden w/out being tripped
    field(CALC, "A:=A!=B;E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
    field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
    field(DOPT, "Use OCAL")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStat_Prev_2-R PP")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:#N1ITLckStatR")
{
    field(DESC, "Calculate combined interlock status")
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_1_TrpR CP")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_1_OvRidnR CP")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_1_HltyR CP")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_1_DisR CP")

# Recalculate Tripped; Check that only one is true
# Clear Tripped if Overriden and Set Tripped if Overriden w/out being tripped
    field(CALC, "A:=A!=B;E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
    field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
    field(DOPT, "Use OCAL")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStat_Next_1-R PP")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:#N2ITLckStatR")
{
    field(DESC, "Calculate combined interlock status")
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_2_TrpR CP")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_2_OvRidnR CP")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_2_HltyR CP")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_Next_2_DisR CP")

# Recalculate Tripped; Check that only one is true
# Clear Tripped if Overriden and Set Tripped if Overriden w/out being tripped
    field(CALC, "A:=A!=B;E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
    field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
    field(DOPT, "Use OCAL")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStat_Next_2-R PP")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#OITLckStatR")
{
    field(DESC, "Calculate combined interlock status")
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_TrpR CP")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_OvRidnR CP")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_HltyR CP")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_Open_DisR CP")

# Recalculate Tripped; Check that only one is true
# Clear Tripped if Overriden and Set Tripped if Overriden w/out being tripped
    field(CALC, "A:=A!=B;E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
    field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
    field(DOPT, "Use OCAL")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStat_Open-R PP")
}

record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStat_Prev_1-R")
{
    field(DESC, "Combined Interlock status")
    field(ZRST, "INVALID")
    field(ONST, "HEALTHY")
    field(TWST, "TRIPPED")
    field(TWSV, "MAJOR")
    field(THST, "OVERRIDEN")
    field(FRST, "NOT CONFIGURED")
}


record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStat_Prev_2-R")
{
    field(DESC, "Combined Interlock status")
    field(ZRST, "INVALID")
    field(ONST, "HEALTHY")
    field(TWST, "TRIPPED")
    field(TWSV, "MAJOR")
    field(THST, "OVERRIDEN")
    field(FRST, "NOT CONFIGURED")
}


record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStat_Next_1-R")
{
    field(DESC, "Combined Interlock status")
    field(ZRST, "INVALID")
    field(ONST, "HEALTHY")
    field(TWST, "TRIPPED")
    field(TWSV, "MAJOR")
    field(THST, "OVERRIDEN")
    field(FRST, "NOT CONFIGURED")
}


record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStat_Next_2-R")
{
    field(DESC, "Combined Interlock status")
    field(ZRST, "INVALID")
    field(ONST, "HEALTHY")
    field(TWST, "TRIPPED")
    field(TWSV, "MAJOR")
    field(THST, "OVERRIDEN")
    field(FRST, "NOT CONFIGURED")
}

record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStat_Open-R")
{
    field(DESC, "Combined Interlock status")
    field(ZRST, "INVALID")
    field(ONST, "HEALTHY")
    field(TWST, "TRIPPED")
    field(TWSV, "MAJOR")
    field(THST, "OVERRIDEN")
    field(FRST, "NOT CONFIGURED")
}
""")

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:#StatR")
{
    field(DESC, "Calculate valve status")
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:UndefinedR CP")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:OpenR CP")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:ClosedR CP")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:ErrorR CP")
    field(INPE, "[PLCF#INSTALLATION_SLOT]:ValidR CP MSS")

# Recalculate 'undefined' (incorrect in PLC)
    field(CALC, "A:=(B==C);F:=(A || B || C || D);F")
    field(OCAL, "E&&F?(D?4:(A?1:B*2+C*3)):0")
    field(DOPT, "Use OCAL")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:StatR PP MSS")
}


record(mbbi, "[PLCF#INSTALLATION_SLOT]:StatR")
{
    field(DESC, "Valve status")
    field(ZRST, "INVALID")
    field(ONST, "UNDEFINED")
    field(TWST, "OPEN")
    field(THST, "CLOSED")
    field(FRST, "ERROR")
    field(FRSV, "MAJOR")
}
""")


#-############################
#- METADATA
#-############################
#- ERRORS
#-define_metadata("error", MD_SCRIPT='css_code2msg.py', CODE_TYPE='error')
#-
#-add_metadata(99, 'Open Status & Close Status Both Active')
#-add_metadata(49, 'Previous Interlock')
#-add_metadata(48, 'Next Interlock')
#-add_metadata(47, 'Personnel Access Interlock')

#- WARNINGS
#-define_metadata("warning", MD_SCRIPT='css_code2msg.py', CODE_TYPE='warning')
#-
#-add_metadata(99, 'Open/Close command timeout')
